import 'dart:convert';
import 'dart:io';

import 'package:aes_crypt/aes_crypt.dart';
import 'package:dio/dio.dart';
import 'package:epub_viewer/epub_viewer.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';

void main() async {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool loading = false;
  bool loading2 = true;
  Dio dio = new Dio();
  String encryptionpath;
  var decryptionPath;
File f;
  @override
  void initState() {
    super.initState();
    // download();

    _downloadFile("http://almashreqebookstore.com/books/4.epub", "book.epub");
   // encrypt_file(f.path);
  }

  // download() async {
  //
  //   await downloadFile();
  //   // if (Platform.isIOS) {
  //   //   print('download');
  //   //   await downloadFile();
  //   // } else {
  //   //   loading = false;
  //   // }
  // }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: loading2
              ? CircularProgressIndicator()
              : FlatButton(
                  onPressed: () async {

                    print(encryptionpath);
                    decrypt_file(encryptionpath);
                    Directory appDocDir =
                        await getApplicationDocumentsDirectory();
                    print('$appDocDir');

                    String iosBookPath = '${appDocDir.path}/book.epub';
                    print(iosBookPath);
                    String androidBookPath = decryptionPath;//appDocDir.path + "/book.epub"; //'file:///android_asset/chair.epub';
                    EpubViewer.setConfig(
                        themeColor: Theme.of(context).primaryColor,
                        identifier: "iosBook",
                        scrollDirection: EpubScrollDirection.ALLDIRECTIONS,
                        allowSharing: true,
                        enableTts: true,
                        nightMode: true);
                    EpubViewer.open(
                      androidBookPath,
                      //"http://almashreqebookstore.com/books/4.epub",
                      //Platform.isAndroid ? androidBookPath : iosBookPath,
                      lastLocation: EpubLocator.fromJson({
                        "bookId": "2239",
                        "href": "/OEBPS/ch06.xhtml",
                        "created": 1539934158390,
                        "locations": {
                          "cfi": "epubcfi(/0!/4/4[simple_book]/2/2/6)"
                        }
                      }),
                    );

                    // await EpubViewer.openAsset(
                    //   'assets/4.epub',
                    //   lastLocation: EpubLocator.fromJson({
                    //     "bookId": "2239",
                    //     "href": "/OEBPS/ch06.xhtml",
                    //     "created": 1539934158390,
                    //     "locations": {
                    //       "cfi": "epubcfi(/0!/4/4[simple_book]/2/2/6)"
                    //     }
                    //   }),
                    // );
                    // get current locator
                    EpubViewer.locatorStream.listen((locator) {
                      print(
                          'LOCATOR: ${EpubLocator.fromJson(jsonDecode(locator))}');
                    });
                  },
                  child: Container(
                    child: Text('open epub'),
                  ),
                ),
        ),
      ),
    );
  }

//   Future downloadFile() async {
//     print('download1');
//     PermissionStatus permission = await PermissionHandler()
//         .checkPermissionStatus(PermissionGroup.storage);
//
//     if (permission != PermissionStatus.granted) {
//       await PermissionHandler().requestPermissions([PermissionGroup.storage]);
//       await startDownload();
//     } else {
//       await startDownload();
//     }
//   }
//
//   startDownload() async {
//     Directory appDocDir = Platform.isAndroid
//         ? await getExternalStorageDirectory()
//         : await getApplicationDocumentsDirectory();
//
//     String path = appDocDir.path + '/chair.epub';
//     print("pppathhhh: "+path);
//     File file = File(path);
// //    await file.delete();
//
//     if (!File(path).existsSync()) {
//       await file.create();
//       await dio.download(
//         'http://almashreqebookstore.com/books/4.epub',
//         path,
//         deleteOnError: true,
//         onReceiveProgress: (receivedBytes, totalBytes) {
//           print((receivedBytes / totalBytes * 100).toStringAsFixed(0));
//           //Check if download is complete and close the alert dialog
//           if (receivedBytes == totalBytes) {
//             loading = false;
//             setState(() {});
//           }
//         },
//       );
//     } else {
//       loading = false;
//       setState(() {});
//     }
//   }
  static var httpClient = new HttpClient();
  Future<File> _downloadFile(String url, String filename) async {
    var request = await httpClient.getUrl(Uri.parse(url));
    var response = await request.close();
    var bytes = await consolidateHttpClientResponseBytes(response);
    String dir = (await getApplicationDocumentsDirectory()).path;
    File file = new File('$dir/$filename');
    print("fileee: " + file.path.toString());
    setState(() {
      f=file;
    });

    await file.writeAsBytes(bytes);
    encrypt_file(file.path);
    setState(() {
      loading2=false;
    });
    error("success downloaded");
    return file;
  }

  String encrypt_file(String path) {
    AesCrypt crypt = AesCrypt();
    crypt.setOverwriteMode(AesCryptOwMode.on);
    crypt.setPassword('MyPassword1234');
    String encFilepath;
    try {
      encFilepath = crypt.encryptFileSync(path);
      setState(() {
        encryptionpath=encFilepath;
      });
      print('The encryption has been completed successfully.');
      print('Encrypted file: $encFilepath');
      error('Encrypted file: $encFilepath');
    } catch (e) {
      if (e.type == AesCryptExceptionType.destFileExists) {
        print('The encryption has been completed unsuccessfully.');
        print(e.message);
      } else {
        return 'ERROR';
      }
    }
    return "success";
  }
   String decrypt_file(String path) {
    AesCrypt crypt = AesCrypt();
    crypt.setOverwriteMode(AesCryptOwMode.on);
    crypt.setPassword('MyPassword1234');
    String decFilepath;
    try {
      decFilepath = crypt.decryptFileSync(path);
      print('The decryption has been completed successfully.');
      error("The decryption has been completed successfully.");
      print('Decrypted file 1: $decFilepath');
      error('Decrypted file 1: $decFilepath');
      print('File content: ' + File(decFilepath).path);
      setState(() {
        decryptionPath=File(decFilepath).path;
      });
    } catch (e) {
      if (e.type == AesCryptExceptionType.destFileExists) {
        print('The decryption has been completed unsuccessfully.');
        error("The decryption has been completed unsuccessfully.");
        print(e.message);
      }
      else{
        return 'ERROR';
      }
    }
    return decFilepath;
  }
  void error(String errorCode) {
    if (errorCode.isNotEmpty) {
      Fluttertoast.showToast(
          msg: errorCode,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.grey,
          textColor: Colors.white,
          fontSize: 16.0);

    }
  }
}
